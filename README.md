# Repositório de Dados - LEMA-UFPB

## 💡 Bucket para dados abertos

Esse projeto armazena pequenos arquivos de dados usados em projetos gerenciados pelo grupo de pesquisa

---

## 📗 Licença

As informações aqui contidas foram produzidas pelo grupo de pesquisa.

O uso, cópia, a transferência ou a divulgação dessas informações são autorizadas desde que haja citação dos mantenedores.

(c) 2023 DIREITOS RESERVADOS

LABORATÓRIO DE ECONOMIA E MODELAGEM APLICADA - LEMA-UFPB

João Pessoa, PB, Brasil

---

## 📋 Catálogo

- `companies-geocoding.json.gz` - Dados de amostra de estabelecimentos comerciais georreferenciados.

---

## ☢️ Boas práticas

- Nunca salvar dados grandes amostras de dados no Git
- Nunca gravar dados sensíveis ou privados

---

## 👏 Contatos e agradecimentos

- hilton.martins@academico.ufpb.br
- alessio.almeida@academico.ufpb.br
