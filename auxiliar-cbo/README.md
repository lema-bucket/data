# CBO
- Os dados do CBO estão disponibilizados no seguinte endereço:

http://www.mtecbo.gov.br/cbosite/pages/downloads.jsf

- Como possui a necessidade de reCaptcha, os arquivos foram extraídos para o LEMA bucket para facilitar a produção de projetos de ETL que requerem esse tipo de informação.